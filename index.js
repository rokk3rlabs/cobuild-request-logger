const loggerFactory = require('./loggerFactory');

const requestLogger = (options = {}) => {
  
  if(!options.methodsOmitted){
    options.methodsOmitted = ['OPTIONS','HEAD'];
  }
  const Logger = loggerFactory(options.type);
  const loggerInstance = new Logger(options);

  const log = async (request, response) => {
    const { rawHeaders, httpVersion, method, socket, url, body, params, query, headers } = request;
    const { remoteAddress, remoteFamily } = socket;

    if(options.methodsOmitted.includes(method)){
      return;
    }

    const logBody = {
      timestamp: Date.now(),
      rawHeaders,
      body,
      query,
      params,
      httpVersion,
      method,
      referer: headers.referer,
      remoteAddress,
      remoteFamily,
      url
    }

    loggerInstance.log(logBody);
  };
  
  const middleware = async (request, response, next) => {
    try{
      await log(request, response);
    }catch(error){
      console.error('requestLogger error:', error);
    }
    
    return next();
  };

  return middleware;
};

module.exports = requestLogger;