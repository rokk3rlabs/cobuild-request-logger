const DefaultLogger = require('./loggers/default.js');
const KnexLogger = require('./loggers/knex');
const MongoLogger = require('./loggers/mongo');

const classes = {
  DefaultLogger,
  KnexLogger,
  MongoLogger 
};

module.exports = function dynamicClass (name) {
  return classes[name] ? classes[name] : classes['DefaultLogger'];
}