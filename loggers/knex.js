const knex = require('knex');
const logSchema = {
  name: 'auditLog',
  schema: {
    timestamp: {
      type:'string'
    },
    rawHeaders: {
      type:'text'
    },
    body: {
      type:'JSON'
    },
    query: {
      type:'JSON'
    },
    params: {
      type:'JSON'
    },
    httpVersion: {
      type:'string'
    },
    method: {
      type:'string'
    },
    referer: {
      type:'string'
    },
    remoteAddress: {
      type:'string'
    },
    remoteFamily: {
      type:'string'
    },
    url: {
      type:'string'
    }
  }
};

class KnexLogger{

  constructor(options){    
    this.client = knex(options);
    this.schema = logSchema;
  }

  async log(payload){
    try{
      if(!await this.client.schema.hasTable(this.schema.name)){       
        await this.createTable();
      }      
      await this.client(this.schema.name).insert(payload);
    }catch(error){
      console.error('KnexLogger error:', error);
      //console.log(JSON.stringify(payload, null, 2));
    }
    
  }

  async createTable(){
    const self = this;
    return this.client.schema.createTable(this.schema.name, function(table) {
      table.increments('id').primary();
      for (const fieldName in self.schema.schema) {
        if (Reflect.has(self.schema.schema, fieldName)) {
          const fieldConfig = self.schema.schema[fieldName];
          const type = fieldConfig.type.toLowerCase();
          console.log('fieldName:', fieldName, 'type:', type, 'fieldConfig:', fieldConfig);
          const field = (type === 'enu') ? table[type](fieldName, fieldConfig.options) : table[type](fieldName);
          if (fieldConfig.required) {
            field.notNull();
          }
          if (fieldConfig.unique && fieldConfig.unique === true) {
            field.unique();
          }
          if (fieldConfig.default) {
            field.defaultTo(fieldConfig.default);
          }
          if (fieldConfig.foreign) {
            field.unsigned();
            table.foreign(fieldName).references(fieldConfig.foreign);
          }
        }
      }
      table.timestamps(false, true)
      
    });
  }
}

module.exports = KnexLogger;