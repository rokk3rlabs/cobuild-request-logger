const { MongoClient } = require("mongodb");

class MongoLogger{
  
  constructor(options){
    this.client = new MongoClient(options.connectionURL, { useUnifiedTopology: true });
    this.options = options;
    if(!this.options.collection){
      this.options.collection = 'auditLog';
    }
    
  }

  async log(payload){
    try {
      await this.client.connect();
      this.database = this.client.db(this.options.database);
      this.collection = this.database.collection(this.options.collection);
      await this.collection.insertOne(payload);
    } catch(error){
      console.error('MongoLogger error:', error);
      console.log(JSON.stringify(payload, null, 2));
    }
  }
}

module.exports = MongoLogger;