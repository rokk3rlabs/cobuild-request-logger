class DefaultLogger{
  log(payload){
    console.log(JSON.stringify(payload, null, 2));
  }
}

module.exports = DefaultLogger;