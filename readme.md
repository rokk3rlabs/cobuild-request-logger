# Cobuild Request Logger

This module allows to log or store all requests made including their URL, query string params, url params or request body. By default this module prints a JSON string to console, but there are 2 loggers included by default: KnexLogger or MongoLogger, allowing to store the gathered information into the desired database.



## Usage

This module is an express middleware, so can be added as a usual middleware as follows:

```javascript
const requestLogger = require('cobuild-request-logger');
const requestLoggerConfig = {};

  app.use(requestLogger(requestLoggerConfig));
```

With this default configuration, the module will use `DefaultLogger` that basically uses stdout to print a result JSON.

To trigger Knex or Mongo logger, you only need to specify `type` option on the instance of the module. The allowed options included by default are `MongoLogger` or `KnexLogger`.

### KnexLogger

An example of how to configure request logger for knex storage (postgres/mysql/mssql) should be as follows:

```javascript
const requestLogger = require('cobuild-request-logger');
const requestLoggerConfig = {
    type:'KnexLogger',
    client:'pg', //pg for postgres
    connection: {
      host: '<host>',
      user: '<user>',
      password: '<password>',
      database: '<database>'
    },
    pool: {
      min: 0,
      max: databaseConfig.maxConnections
    },
    debug: false
  };

  app.use(requestLogger(requestLoggerConfig));
```

### MongoLogger

An example of how to configure request logger for mongo storage should be as follows:

```javascript
const requestLogger = require('cobuild-request-logger');
const requestLoggerConfig = {
    type:'MongoLogger',
    database: '<database>',
    connectionURL:'<connectionURL>'
  };

  app.use(requestLogger(requestLoggerConfig));
```
